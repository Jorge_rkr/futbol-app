import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { map } from 'rxjs/operators'
import { UsuarioModel } from '../models/usuario.model';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private url = 'https://identitytoolkit.googleapis.com/v1/accounts:';
  private apikey = 'AIzaSyDOJb0M4vaAkHDYZE3DaeMimFDQrZSaJn8';

  userToken: string;
  public usuario: any = {};

  constructor(private http: HttpClient,
              private afs: AngularFirestore,
              public auth: AngularFireAuth) {
    this.leerToken();
   /*  this.auth.authState.subscribe( user => {
      console.log('estado del usuario: ',  user);

      if ( !user) {
        return;
      } else {
        this.usuario.nombre = user.displayName;
        this.usuario.uid = user.uid;
      }
    }); */
  }

  /* loginSocial( proveedor: string ) {
    if ( proveedor === 'google'){
      this.auth.signInWithPopup(new auth.GoogleAuthProvider());
    } else {
      this.auth.signInWithPopup(new auth.TwitterAuthProvider());
    }
  }
  
  logoutSocial() {
    this.usuario = {};
    this.auth.signOut();
  } */


  logOut(){
    localStorage.removeItem('token');
    localStorage.removeItem('expira');
  }

  login( usuario: UsuarioModel ){
    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    localStorage.setItem('nuevoRegistro', 'NO');
    
    return this.http.post(
      `${this.url}signInWithPassword?key=${this.apikey}`,
      authData
    ).pipe(
      map( resp => {
        this.guardarToken( resp['idToken'] );
        return resp;
      })
    );

  }

  nuevoUsuario( usuario: UsuarioModel ){

    const authData = {
      ...usuario,
      returnSecureToken: true
    };

    return this.http.post(
      `${this.url}signUp?key=${this.apikey}`,
      authData
    ).pipe(
      map( resp => {
        this.guardarToken( resp['idToken'] );
        return resp;
      })
    );

  }

  private guardarToken( idToken: string ){
    this.userToken = idToken;
    localStorage.setItem('token', idToken);

    let hoy = new Date();
    hoy.setSeconds( 3600 );

    localStorage.setItem( 'expira', hoy.getTime().toString() );
  }

  leerToken(){
    if ( localStorage.getItem('token') ) {
      this.userToken = localStorage.getItem('token');
    } else {
      this.userToken = '';
    }

    return this.userToken;
  }

  estaAutenticado(): boolean {
    if ( this.userToken.length < 2 ){
      return false;
    }
    const expira =   Number(localStorage.getItem( 'expira'));
    const expiraDate = new Date();
    expiraDate.setTime( expira );

    if ( expiraDate > new Date() ){

      if ( localStorage.getItem('expira') && localStorage.getItem('nuevoRegistro') !== 'SI' ) {
          return true;
      } else {
        return false;
      }
      
    } else {
      return false;
    }

  }
}
