import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FutbolistaModel } from '../models/futbolista.model';
import { delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FutbolistasService {

  private url = 'https://futbol-app-ed160.firebaseio.com/';

  constructor(private http: HttpClient) { }

  crearHeroe( futbolista: FutbolistaModel ){
    return this.http.post(`${ this.url }/futbolistas.json`, futbolista)
    .pipe(
      map( (resp: any) => {
        futbolista.id = resp.name;
        return futbolista;
      })
    );
  }


  actualizarFutbolista( futbolista: FutbolistaModel ){

    const futTemp = {
      ...futbolista
    };
    
    delete futTemp.id;

    return this.http.put(`${ this.url }/futbolistas/${ futbolista.id }.json`, futTemp);
  }

  borrarFutbolista( id: string ){
    return this.http.delete(`${ this.url }/futbolistas/${ id }.json`);
  }

  getFutbolista(id: string ){
    return this.http.get(`${ this.url }/futbolistas/${ id }.json`);
  }

  obtenerFutbolistas(){
    return this.http.get(`${ this.url }/futbolistas.json`)
    .pipe(
      map( resp => this.crearArreglo( resp ) ),
      delay(1200)
    );
  }

  private crearArreglo( futblistasObj: object ){

    const futbolistas: FutbolistaModel[] = [];
    Object.keys( futblistasObj ).forEach( key => {
    
      const heroe: FutbolistaModel = futblistasObj[key];
      heroe.id = key;
      futbolistas.push( heroe );
    });
    
    if ( futblistasObj === null ) { return []; }
    return futbolistas;
  }

  
}
