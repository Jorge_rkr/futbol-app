import { Component, OnInit } from '@angular/core';
import { FutbolistaModel } from 'src/app/models/futbolista.model';
import { FutbolistasService } from 'src/app/services/futbolistas.service';
import { ActivatedRoute  } from '@angular/router';
import { NgForm } from '@angular/forms';
import Swal from 'sweetalert2';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-futbolista',
  templateUrl: './futbolista.component.html'
})
export class FutbolistaComponent implements OnInit {

  // Inicializo una estancia del model futbolista
  futbolista: FutbolistaModel = new FutbolistaModel();
  
  // Declaración de servicios
  constructor(private _futbolistasService: FutbolistasService,
              private router: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.router.snapshot.paramMap.get('id');

    if ( id !== 'nuevo') {
      this._futbolistasService.getFutbolista( id )
      .subscribe( (resp: FutbolistaModel) => {
        this.futbolista = resp;
        this.futbolista.id = id;
      });
    } 
  }

  guardar( form: NgForm ){

    console.log( form );
    
    // Si el formulario no es valido, no ejecuta nada
    if ( form.invalid ) {
      console.log('formulario no valido');
      return;
    }

    // Se lanza sweet alert
    Swal.fire({
      title: 'Espere',
      text: 'Guardando información',
      icon: 'info',
      allowOutsideClick: false
    });
    Swal.showLoading();
    
    let peticion: Observable<any>;
    let mensajeConfirmacion = '';

    // Dependiendo si viene el ID edita o crea
    if ( this.futbolista.id ) {
      peticion = this._futbolistasService.actualizarFutbolista( this.futbolista ); 
      mensajeConfirmacion = 'Se edito correctamente el registro'; 
    } else {
      peticion = this._futbolistasService.crearHeroe( this.futbolista );
      mensajeConfirmacion = 'Se creo correctamente el registro';
    }
    
    // Cuando la acción se ejecuta se lanza, msj de confirmación
    peticion.subscribe( resp => {
      Swal.fire({
        title: this.futbolista.nombre,
        text: mensajeConfirmacion,
        icon: 'success'
      });
    });
   
  }

}

