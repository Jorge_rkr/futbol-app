import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  auth: AuthService;

  constructor(private authService: AuthService,
              private router: Router) {
      this.auth = this.authService;
    }

  ngOnInit(): void {
  }

  salir(){
    this.auth.logOut();
    // TODO
    //this.auth.logoutSocial();
    this.router.navigateByUrl('/login');
  }
}
