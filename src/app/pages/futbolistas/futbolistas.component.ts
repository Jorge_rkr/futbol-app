import { Component, OnInit } from '@angular/core';
import { FutbolistaModel } from 'src/app/models/futbolista.model';
import { FutbolistasService } from 'src/app/services/futbolistas.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-futbolistas',
  templateUrl: './futbolistas.component.html'
})
export class FutbolistasComponent implements OnInit {

  futbolistas: FutbolistaModel[] = [];
  cargando = false;
 
  constructor(private _futbolistasService: FutbolistasService) {

  }

  ngOnInit(): void {
    this.cargando = true;
    this._futbolistasService.obtenerFutbolistas().subscribe( resp => {
      console.log(resp);
      this.futbolistas = resp;
      this.cargando = false;
    });
  }

  borrarFutbolista( futbolista: FutbolistaModel, i: number ){
    Swal.fire({
      title: '¿Esta seguro?',
      text: `Está seguro que desea borrar a ${ futbolista.nombre }`,
      icon: 'question',
      showConfirmButton: true,
      showCancelButton: true
    }).then( resp => {
      if ( resp.value ){
        this.futbolistas.splice(i, 1);
        this._futbolistasService.borrarFutbolista( futbolista.id ).subscribe();
      }
    });
  }

}
