import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { FutbolistaComponent } from './pages/futbolista/futbolista.component';
import { FutbolistasComponent } from './pages/futbolistas/futbolistas.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { RegistroComponent } from './pages/registro/registro.component';

const routes: Routes = [
  { 
    path: 'home',
    component: HomeComponent,
    canActivate: [ AuthGuard ]
  },
  { 
    path: 'futbolistas', 
    component: FutbolistasComponent,
    canActivate: [ AuthGuard ]
  },
  { 
    path: 
    'futbolista/:id', 
    component: FutbolistaComponent,
    canActivate: [ AuthGuard ]
  },
  { path: 'registro', component: RegistroComponent },
  { path: 'login'   , component: LoginComponent },
  { path: '**', redirectTo: 'login' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
