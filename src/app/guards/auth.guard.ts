import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  public usuario: any = {};
  
  constructor(private auth: AuthService,
              private router: Router,
              public authF: AngularFireAuth) {
    
  }
  
  canActivate(): boolean {
    if ( this.auth.estaAutenticado() ) {  
      return true;
    } else {
      this.router.navigateByUrl('/login');
      return false;
    }
  }
}
