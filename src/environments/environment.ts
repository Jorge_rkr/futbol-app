// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDOJb0M4vaAkHDYZE3DaeMimFDQrZSaJn8",
    authDomain: "futbol-app-ed160.firebaseapp.com",
    databaseURL: "https://futbol-app-ed160.firebaseio.com",
    projectId: "futbol-app-ed160",
    storageBucket: "futbol-app-ed160.appspot.com",
    messagingSenderId: "232408766606",
    appId: "1:232408766606:web:c2ccc270258117bc4551df"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
